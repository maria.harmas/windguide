package ee.bcs.koolitus.windguide.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.windguide.dao.InsertedWind;
import ee.bcs.koolitus.windguide.resource.LocationDataResource;

@Path("/locationData")
public class LocationDataController {
	@GET
	@Path("/{insertedWindId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<InsertedWind> getInsertedWindByLocationData(@PathParam("insertedWindId") int insertedWindId) {
		return LocationDataResource.getLocationData(insertedWindId);
	}
}
