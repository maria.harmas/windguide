package ee.bcs.koolitus.windguide.controller;

import java.util.Set;

import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.windguide.dao.InsertedWind;
import ee.bcs.koolitus.windguide.resource.InsertedWindResource;

@Path("/insertedWind")
public class InsertedWindController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Set<InsertedWind> getAllInsertedWinds() {
		return InsertedWindResource.getAllInsertedWinds();

	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public InsertedWind addNewInsertedWind(InsertedWind insertedWind) {
		return InsertedWindResource.addInsertedWind(insertedWind);

	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateInsertedWind(InsertedWind insertedWind) {
		InsertedWindResource.updateInsertedWind(insertedWind);

	}

	/*
	 * @DELETE
	 * 
	 * @Path("/{locationId}") public void
	 * deleteLocationByLocationId(@PathParam("locationId")int locationId) {
	 * LocationResource.deleteLocationByLocationId(locationId);
	 */

	/*
	 * @DELETE
	 * 
	 * @Consumes public void deleteInsertedWind (InsertedWind insertedWind) {
	 * InsertedWindResource.deleteInsertedWind(insertedWind); }
	 */
}
