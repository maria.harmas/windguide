package ee.bcs.koolitus.windguide.controller;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.windguide.dao.Location;
import ee.bcs.koolitus.windguide.resource.LocationResource;

@Path("/location")
public class LocationController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Set<Location> getAllLocations() {
		return LocationResource.getAllLocations();

	}

	@GET
	@Path("/{locationId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Location getLocationByLocationId(@PathParam("locationId") int locationId) {
		return LocationResource.getLocationById(locationId);

	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Location addNewLocation(Location location) {
		return LocationResource.addLocation(location);

	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateLocation(Location location) {
		LocationResource.updateLocation(location);

	}

	/*
	 * @DELETE
	 * 
	 * @Path("/{locationId}") public void
	 * deleteLocationByLocationId(@PathParam("locationId")int locationId) {
	 * LocationResource.deleteLocationByLocationId(locationId);
	 */

	@DELETE
	@Consumes
	public void deleteLocation(Location location) {
		LocationResource.deleteLocation(location);
	}
}
