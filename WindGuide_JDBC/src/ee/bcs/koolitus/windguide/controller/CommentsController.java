package ee.bcs.koolitus.windguide.controller;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.windguide.dao.Comment;
import ee.bcs.koolitus.windguide.resource.CommentsResource;


@Path("/comments")
public class CommentsController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Set<Comment> getAllComments() {
		return CommentsResource.getAllComments();

	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Comment addNewComment(Comment comment) {
		return CommentsResource.addComment(comment);

	}
}
