package ee.bcs.koolitus.windguide.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.LinkedHashSet;

import java.util.Set;

import ee.bcs.koolitus.windguide.dao.Comment;

public abstract class CommentsResource {
	public static Set<Comment> getAllComments() {
		Set<Comment> comments = new LinkedHashSet<>();
	
		 /*"SELECT comment_wind as commentary, time_wind as time FROM inserted_wind "
				+ "UNION ALL SELECT comment as commentary, comment_time as time FROM comment ORDER BY time DESC LIMIT 14";*/
				
		String sqlQuery = "SELECT concat(location.location_name,', tuul ',inserted_wind,' m/s;  ',comment_wind) as commentary, "
				+ "time_wind as time FROM inserted_wind join location on location.id = inserted_wind.location_id "
				+ "UNION ALL SELECT comment as commentary, comment_time as time FROM comment "
				+ "ORDER BY time DESC LIMIT 14";
		
		
		
		
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				Comment comment = new Comment().setComment(results.getString("commentary"))
						.setCommentTime(results.getTimestamp("time"));

				comments.add(comment);

			}
		} catch (

		SQLException e) {
			System.out.println("Error on getting location set: " + e.getMessage());
		}
		return comments;

	}

	public static Comment addComment(Comment comment) {
		String sqlQuery = "INSERT INTO comment (comment) " + "VALUES ('" + comment.getComment() + "')";

		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				comment.setId(resultSet.getInt(1));

			}

		} catch (SQLException e) {
			System.out.println("Error on adding location: " + e.getMessage());
		}
		return comment;
	}

}
