package ee.bcs.koolitus.windguide.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import ee.bcs.koolitus.windguide.dao.Location;

public abstract class LocationResource {
	public static Set<Location> getAllLocations() {
		Set<Location> locations = new HashSet<>();
		String sqlQuery = "SELECT * FROM location";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				Location location = new Location().setId(results.getInt("id"))
						.setLocationName(results.getString("location_name"))
						.setLatitude(results.getBigDecimal("lat"))
						.setLongitude(results.getBigDecimal("lng"));
				locations.add(location);

			}
		} catch (

		SQLException e) {
			System.out.println("Error on getting location set: " + e.getMessage());
		}
		return locations;

	}

	public static Location getLocationById(int locationId) {
		Location location = null;
		// kasutan Stringi, et anda sql käsk
		String sqlQuery = "SELECT * FROM location WHERE id=" + locationId;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				location = new Location().setId(results.getInt("id"))
						.setLocationName(results.getString("location_name"))
						.setCountyName(results.getString("location_county"))
						.setPostalCode(results.getInt("postal_code"));
						//.setCoordinates(results.getString("location_coordinates"));
			}
		} catch (

		SQLException e) {
			System.out.println("Error on getting location set: " + e.getMessage());
		}
		return location;

	}

	// muudame sedasama 3 id aadressi
	public static  Location updateLocation(Location location) {

		// kasutan Stringi, et anda sql käsk

		String sqlQuery = "UPDATE location SET location_name= '" + location.getLocationName() + "' ,location_county= '"
				+ location.getCountyName() + "' ,postal_code= '" + location.getPostalCode()
				+ "' ,measured_wind= '" + location.getMeasuredWind();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on updating location");
			}

		} catch (

		SQLException e) {
			System.out.println("Error on getting location set: " + e.getMessage());
		}
		return location;
	}

	public static void deleteLocation(Location location) {

		String sqlQuery = "DELETE from location WHERE id= " + location.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on updating location");
			}

		} catch (

		SQLException e) {
			System.out.println("Error on getting location set: " + e.getMessage());
		}

	}

	public static Location addLocation(Location location) {
		String sqlQuery = "INSERT INTO location (location_name, location_county, lat, lng, postal_code, measured_wind) "
				+ "VALUES ('" + location.getLocationName() + "','" + location.getCountyName()
				+ "','" + location.getLatitude()+ "','" + location.getLongitude()+ "','"
				+ location.getPostalCode() + "',' " + location.getMeasuredWind() + "')";

		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				location.setId(resultSet.getInt(1));

			}

		} catch (SQLException e) {
			System.out.println("Error on adding location: " + e.getMessage());
		}
		return location;
	}
}
