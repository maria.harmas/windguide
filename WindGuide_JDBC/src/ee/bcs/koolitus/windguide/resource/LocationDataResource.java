package ee.bcs.koolitus.windguide.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.windguide.dao.InsertedWind;

public abstract class LocationDataResource {

	public static List<InsertedWind> getLocationData(int locationId) {
		List<InsertedWind> winds = new ArrayList<>();

		String sqlQuery = "SELECT * FROM inserted_wind WHERE location_id=" + locationId
				+ " ORDER BY time_wind DESC LIMIT 5";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				InsertedWind locationWind = new InsertedWind().setId(results.getInt("id"))
						.setInsertedWind(results.getDouble("inserted_wind"))
						.setCommentWind(results.getString("comment_wind")).setTime(results.getTimestamp("time_wind"))
						.setLocation(LocationResource.getLocationById(results.getInt("location_id")));
				winds.add(locationWind);

			}
		} catch (

		SQLException e) {
			System.out.println("Error on getting inserted wind set: " + e.getMessage());
		}
		return winds;

	}
}
