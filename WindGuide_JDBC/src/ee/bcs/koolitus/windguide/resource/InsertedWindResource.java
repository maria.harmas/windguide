package ee.bcs.koolitus.windguide.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.LinkedHashSet;
import java.util.Set;

import ee.bcs.koolitus.windguide.dao.InsertedWind;

public abstract class InsertedWindResource {
	public static Set<InsertedWind> getAllInsertedWinds() {
		Set<InsertedWind> insertedWinds = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM inserted_wind ORDER BY time_wind DESC LIMIT 10";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				InsertedWind insertedWind = new InsertedWind().setId(results.getInt("id"))
						.setInsertedWind(results.getDouble("inserted_wind"))
						.setCommentWind(results.getString("comment_wind"))
						.setLocation(LocationResource.getLocationById(results.getInt("location_id")))
						.setTime(results.getTimestamp("time_wind"));

				insertedWinds.add(insertedWind);

			}
		} catch (

		SQLException e) {
			System.out.println("Error on getting inserted wind set: " + e.getMessage());
		}
		return insertedWinds;
	}

	public static InsertedWind getInsertedWindById(int insertedWindId) {
		InsertedWind insertedWind = null;

		String sqlQuery = "SELECT * FROM location WHERE id=" + insertedWindId;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				insertedWind = new InsertedWind().setId(results.getInt("id"))
						.setInsertedWind(results.getDouble("inserted_wind"))
						.setCommentWind(results.getString("comment_wind")).setTime(results.getTimestamp("time_wind"))
						.setLocation(LocationResource.getLocationById(results.getInt("location_id")));

			}
		} catch (

		SQLException e) {
			System.out.println("Error on getting inserted wind set: " + e.getMessage());
		}
		return insertedWind;

	}

	public static InsertedWind updateInsertedWind(InsertedWind insertedWind) {

		String sqlQuery = "UPDATE insertedWind SET inserted_wind= '" + insertedWind.getInsertedWind()
				+ "' ,comment_wind= '" + insertedWind.getCommentWind() + "' ,location_id= '"
				+ insertedWind.getLocation().getId() + "' ,time_wind= '" + insertedWind.getTime() + "')";

		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on updating insertedWind");
			}

		} catch (

		SQLException e) {
			System.out.println("Error on getting location set: " + e.getMessage());
		}
		return insertedWind;
	}

	public static InsertedWind addInsertedWind(InsertedWind insertedWind) {
		String sqlQuery = "INSERT INTO inserted_wind (inserted_wind, comment_wind, location_id) " + "VALUES ('"
				+ insertedWind.getInsertedWind() + "','" + insertedWind.getCommentWind() + "','" + insertedWind.getId()
				+ "')";

		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				insertedWind.setId(resultSet.getInt(1));

			}

		} catch (SQLException e) {
			System.out.println("Error on adding location: " + e.getMessage());
		}
		return insertedWind;
	}
}
