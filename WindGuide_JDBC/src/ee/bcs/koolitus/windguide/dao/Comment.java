package ee.bcs.koolitus.windguide.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.lang.String;

public class Comment {
private int id;
private String comment;
private Timestamp commentTime

;{
	
}

public int getId() {
	return id;
}

public Comment setId(int id) {
	this.id = id;
	return this;
}

public String getComment() {
	return comment;
}

public Comment setComment(String comment) {
	this.comment = comment;
	return this;
}

public Timestamp getCommentTime() {
	return commentTime;
}

public Comment setCommentTime(Timestamp commentTime) {
	this.commentTime = commentTime;
	return this;
}




}
