package ee.bcs.koolitus.windguide.dao;

import java.sql.Timestamp;

public class InsertedWind {
	private int id;
	private double insertedWind;
	private String commentWind;
	private Location location;
	private Timestamp time;

	public int getId() {
		return id;
	}

	public InsertedWind setId(int id) {
		this.id = id;
		return this;
	}

	public double getInsertedWind() {
		return insertedWind;
	}

	public InsertedWind setInsertedWind(double insertedWind) {
		this.insertedWind = insertedWind;
		return this;
	}

	public String getCommentWind() {
		return commentWind;
	}

	public InsertedWind setCommentWind(String commentWind) {
		this.commentWind = commentWind;
		return this;
	}

	public Location getLocation() {
		return location;
	}

	public InsertedWind setLocation(Location location) {
		this.location = location;
		return this;

	}

	public Timestamp getTime() {
		return time;

	}

	public InsertedWind setTime(Timestamp time) {
		this.time = time;
		return this;
	}

}
