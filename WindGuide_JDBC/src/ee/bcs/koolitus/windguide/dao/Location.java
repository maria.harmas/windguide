package ee.bcs.koolitus.windguide.dao;

import java.math.BigDecimal;

public class Location {

	private int id;
	private String locationName;
	private String countyName;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private int postalCode;
	private BigDecimal measuredWind;

	public int getId() {
		return id;
	}

	public Location setId(int id) {
		this.id = id;
		return this;
	}

	public String getLocationName() {
		return locationName;
	}

	public Location setLocationName(String locationName) {
		this.locationName = locationName;
		return this;
	}

	public String getCountyName() {
		return countyName;
	}

	public Location setCountyName(String countyName) {
		this.countyName = countyName;
		return this;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public Location setPostalCode(int postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	
	public BigDecimal getLatitude() {
		return latitude;
	}

	public Location setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
		return this;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public Location setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
		return this;
	}

	public BigDecimal getMeasuredWind() {
		return measuredWind;
	}

	public Location setMeasuredWind(BigDecimal measuredWind) {
		this.measuredWind = measuredWind;
		return this;
	}

	@Override
	public String toString() {
		return "Location[Id=" + id + ", location_name=" + locationName + ", location_county=" + countyName
				+ " ,latitude=" + latitude + " ,longitude=" + longitude + " , postal_code=" + postalCode 
				+ " , measured_wind= " + measuredWind + "]";
	}

}
