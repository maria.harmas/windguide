var allLocations;
function getAllLocations() {
	$.getJSON("http://localhost:8080/WindGuide/rest/location", function(
			locations) {
		allLocations = locations;
		console.log(locations);
		var tableBody = document.getElementById("locationTableBody");
		var tableContent = "";
		for (var i = 0; i < locations.length; i++) {
			tableContent = tableContent + "<tr><td>"
					+ locations[i].locationName + "</td><td>"
					+ locations[i].latitude + "</td><td>"
					+ locations[i].longitude 
					+ "</td></tr>";
		}
		tableBody.innerHTML = tableContent;
	});

}
getAllLocations();

function addNewInsertedWind() {
	var wind = document.getElementById("insertedWind").value;
	
	if (wind == "") {
		alert("Sisesta tuul!");
	} else {
		
	
	
	var newInsertedWindJson = {
		"insertedWind" : wind,
		"commentWind" : document.getElementById("commentWind").value,
		"id" : document.getElementById("locationId").value
		}
	console.log("uus tuul: " + newInsertedWindJson);
	var newInsertedWind = JSON.stringify(newInsertedWindJson);

	$.ajax({
		url : "http://localhost:8080/WindGuide/rest/insertedWind",
		type : "POST",
		data : newInsertedWind,
		contentType : "application/json; charset=utf-8",
		success : function() {
			alert("Aitäh!");
			getAllInsertedWinds();

		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}
}

function getAllInsertedWinds() {
	$.getJSON("http://localhost:8080/WindGuide/rest/insertedWind", function(
			insertedWinds) {
		allInsertedWinds = insertedWinds;
		console.log(insertedWinds);
		var tableBody = document.getElementById("InsertedWindTableBody");
		var tableContent = "";

		for (var i = 0; i < insertedWinds.length; i++) {
			var timestamp = (insertedWinds[i].time);

			var date = new Date(timestamp);

			tableContent = tableContent + "<tr><td>"
					+ insertedWinds[i].insertedWind + " m/s" + "</td><td>"
					+ insertedWinds[i].commentWind + "</td><td>"
					+ insertedWinds[i].location.locationName + "</td><td>"
					+ date.toLocaleDateString() + " "
					+ date.toLocaleTimeString() + "</td></tr>";

		}
		tableBody.innerHTML = tableContent;

	});

}
getAllInsertedWinds();

function getAllComments() {
	$.getJSON("http://localhost:8080/WindGuide/rest/comments", function(
			comments) {
		allComments = comments;
		console.log(comments);
		var tableBody = document.getElementById("CommentTableBody");
		var tableContent = "";
		for (var i = 0; i < comments.length; i++) {

			var timestamp = (comments[i].commentTime);
			var date = new Date(timestamp);

			tableContent = tableContent + "<tr><td>" + comments[i].comment
					+ "</td><td>" + date.toLocaleDateString() + " "
					+ date.toLocaleTimeString() + "</td></tr>";

		}
		tableBody.innerHTML = tableContent;
	});
}
getAllComments();

function addNewComment() {
	
	var comment = document.getElementById("comments").value;
	if (comment == "") {
		alert("Tühi kommentaar!");
	} else {
		
	
	var newCommentJson = {
		"comment" : comment

	}
	console.log("uus kommentaar: " + newCommentJson);
	var newComment = JSON.stringify(newCommentJson);

	$.ajax({
		url : "http://localhost:8080/WindGuide/rest/comments",
		type : "POST",
		data : newComment,
		contentType : "application/json; charset=utf-8",
		success : function() {
			alert("Aitäh!");
			getAllComments();

		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
	}
}

function getAllLocationDatas() {
	var selectedLocation = document.getElementById("locationId").value;
	if (selectedLocation != "") {
		$.getJSON("http://localhost:8080/WindGuide/rest/locationData/"
				+ selectedLocation, function(locationWinds) {
			allLocationWinds = locationWinds;
			console.log(locationWinds);
			var tableBody = document.getElementById("LocationWindsTableBody");
			var tableContent = "";

			for (var i = 0; i < locationWinds.length; i++) {

				var timestamp = (locationWinds[i].time);

				var date = new Date(timestamp);
				tableContent = tableContent + "<tr><td>"
						+ locationWinds[i].insertedWind + " m/s" + "</td><td>"

						+ date.toLocaleDateString() + " "
						+ date.toLocaleTimeString() + "</td></tr>";

			}
			tableBody.innerHTML = tableContent;
$("#tuuleTabel").show();
		});

	}
}

